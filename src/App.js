import React, {Component} from 'react';
import './App.css';

import MaterialTable from "material-table";

import { forwardRef } from 'react';
import { ArrowUpward, Search, LastPage, FirstPage, Clear, DeleteOutline, FilterList, ChevronRight, ChevronLeft } from "@material-ui/icons";

class App extends Component {
  constructor() {
    super()
    this.state = {
      hasErrors: false,
      errorMessage: 'Something went wrong',
      takers: []
    }
  };

  componentDidMount() {
    fetch(process.env.REACT_APP_API_HOST+"/takers/list.json")
      .then(response => response.json())
      .then(data => {
        if (data.message) {
          this.setState({
            errorMessage: data.message,
            hasErrors: true
          })
        } else {
          this.setState({ takers: data.data })
        }
      })
      .catch(e => {
        console.log(e)
        this.setState({ hasErrors: true })
      });
  }

  render() {
      return (
        <div style={{margin: 0, padding: 0}}>
          <div id="message" style={{display: this.state.hasErrors ? 'block' : 'none'}}><strong>{this.state.errorMessage}</strong></div>
          <MaterialTable
            title="Test Takers"
            columns={[
              { title: 'Login', field: 'login' },
              {
                title: 'Password',
                field: 'password',
                render: rowData => (
                  <p>********</p>
                )
              },
              {
                title: 'Name',
                field: 'title',
                render: rowData => (
                  <p style={{textTransform: 'capitalize'}}>{rowData.title} {rowData.firstname} {rowData.lastname}</p>
                ),
               },
              { title: 'Gender', field: 'gender' },
              {
                title: 'Email',
                field: 'email',
                render: rowData => (
                  <a style={{color: 'black'}} href="#" onClick={()=>window.open("mailto:"+rowData.email)}><strong>{rowData.email}</strong></a>
                ),
              },
              {
                title: 'Avatar',
                field: 'picture',
                render: rowData => (
                  <img
                    style={{ height: 36, borderRadius: '50%' }}
                    src={rowData.picture}
                  />
                ),
              },
              { title: 'Address', field: 'address' }

            ]}
            data={this.state.takers}
            options={{
              search: true,
              pageSize: 10
            }}
            icons={{
              Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
              Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
              Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
              FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
              LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
              NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
              PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
              ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
              Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
              SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
            }}
          />
        </div>
    )
  }
}

export default App;
